import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {PreTest2Component} from "./components/pre-test2/pre-test2.component";
import {PreTest1Component} from "./components/pre-test1/pre-test1.component";

const routes: Routes = [
  {path: 'pre-test1', component: PreTest1Component},
  {path: 'pre-test2', component: PreTest2Component},
];


@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }

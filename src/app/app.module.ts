import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {ReactiveFormsModule} from "@angular/forms";
import { PreTest2Component } from './components/pre-test2/pre-test2.component';
import { AppRoutingModule } from './app-routing.module';
import { PreTest1Component } from './components/pre-test1/pre-test1.component';


@NgModule({
  declarations: [
    AppComponent,
    PreTest2Component,
    PreTest1Component,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

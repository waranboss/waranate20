import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreTest1Component } from './pre-test1.component';

describe('PreTest1Component', () => {
  let component: PreTest1Component;
  let fixture: ComponentFixture<PreTest1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreTest1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreTest1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

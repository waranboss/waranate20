import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {Router} from "@angular/router";
import {isNumeric} from "rxjs/internal-compatibility";

@Component({
  selector: 'app-pre-test1',
  templateUrl: './pre-test1.component.html',
  styleUrls: ['./pre-test1.component.css']
})
export class PreTest1Component implements OnInit {


  title = 'preTest';
  inputForm!: FormGroup;
  answer: string | undefined;

  ngOnInit(): void {
    this.inputForm = new FormGroup({
      'inputNumber': new FormControl(null),
      'numberType': new FormControl('p'),
      'answer': new FormControl(null),
    });
  }

  constructor(
    private router: Router,) {

  }
  onCheck() {
    if (this.inputForm.value.numberType == 'p') {

      console.log('ppp');
      if(!this.checkPrime(this.inputForm.value.inputNumber)){
        this.answer = "False";
      }else this.answer = "True";
    }

    else if (this.inputForm.value.numberType == 'f'){
      console.log('ff');
      let fibo = this.checkFibo(this.inputForm.value.inputNumber);
      if(fibo.some(x => x == this.inputForm.value.inputNumber)){
        this.answer = fibo.toString();
      }else {
        this.answer = '-';
      }
    }

    return null
  }

  checkPrime(num: number){
    for (let i = 2; i < num; i++)
      if (num % i === 0) return false;
    return num > 1;
  }

  checkFibo(num: number){
    let fibonacci = [0,1];
    for (let i = 1; i < num; i++) {
      fibonacci.push(fibonacci[i] + fibonacci[i - 1]);
    }
    return fibonacci;
  }

  onConvertNumber() {

    if (!isNumeric(this.inputForm.value.inputNumber)){
      this.inputForm.value.inputNumber.resetForm();
    }
    else if (Number(this.inputForm.value.inputNumber) < 0){
      this.inputForm = new FormGroup({
        'inputNumber': new FormControl(1)});
    }
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreTest2Component } from './pre-test2.component';

describe('PreTest2Component', () => {
  let component: PreTest2Component;
  let fixture: ComponentFixture<PreTest2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreTest2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreTest2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

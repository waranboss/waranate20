import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-pre-test2',
  templateUrl: './pre-test2.component.html',
  styleUrls: ['./pre-test2.component.css']
})
export class PreTest2Component implements OnInit {
  result: any;
  resultStorage: any;
  searchForm!: FormGroup;

  constructor() { }

  ngOnInit(): void {
    this.getData();
    this.searchForm = new FormGroup({
      'searchName': new FormControl(null),
    });


  }

  getData(){
    fetch(' https://api.publicapis.org/categories').then(res => res.json()).then((response) =>    {
      this.result = response;
      this.resultStorage = response
    });
    return this.result;
  }

  onSearch(){
    this.result = this.resultStorage.filter((data: any) => data.toLowerCase().includes(this.searchForm.value.searchName.toLowerCase()));
  }

}


